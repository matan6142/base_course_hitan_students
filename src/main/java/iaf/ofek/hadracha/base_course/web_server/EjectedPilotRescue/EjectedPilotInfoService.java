package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.InMemoryMapDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EjectedPilotInfoService {
    @Autowired
    private InMemoryMapDataBase db;

    @Autowired
    private AirplanesAllocationManager airplanesAllocationManager;

    public EjectedPilotInfo rescuePilot(int ejectionId, String clientId) {
        EjectedPilotInfo pilotToRescue = db.getByID(ejectionId, EjectedPilotInfo.class);
        if (!pilotToRescue.isRescued()) {
            airplanesAllocationManager.allocateAirplanesForEjection(pilotToRescue, clientId);
            pilotToRescue.setRescuedBy(clientId);
            db.update(pilotToRescue);
        }
        return pilotToRescue;
    }

    public List<EjectedPilotInfo> getPilotsToRescue() {
        return db.getAllOfType(EjectedPilotInfo.class);
    }
}
