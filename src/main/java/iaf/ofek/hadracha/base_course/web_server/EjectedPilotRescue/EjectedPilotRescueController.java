package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EjectedPilotRescueController {

    @Autowired
    private EjectedPilotInfoService ejectedPilotInfoService;

    @GetMapping("/ejectedPilotRescue/infos")
    public List<EjectedPilotInfo> getEjectedPilots() {
        return ejectedPilotInfoService.getPilotsToRescue();
    }

    @GetMapping("/ejectedPilotRescue/takeResponsibility")
    public EjectedPilotInfo rescuePilot(@RequestParam int ejectionId, @CookieValue("client-id") String clientid) {
        return ejectedPilotInfoService.rescuePilot(ejectionId, clientid);
    }
}
